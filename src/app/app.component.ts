import { Component, ViewChild } from '@angular/core';
import { MobileService, ScreenSize } from './services/mobile/mobile.service';
import { EventManager } from '@angular/platform-browser';
import { Directionality } from '@angular/cdk/bidi';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { FolderJsonService } from './services/folder-json.service';

export enum linkEnums {
  HOME = '/',
  FILES = 'files'
}
export const links = [
  {
    text: "דף הבית",
    value: linkEnums.HOME
  },
  {
    text: "כל הקבצים",
    value: linkEnums.FILES
  }
];
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cs2020';
  public screenSize$: ScreenSize = null;
  public links = links;
  @ViewChild('sidenav', { static: false }) sidenav: MatSidenav;
  constructor(
    private mobileService: MobileService,
    private eventManager: EventManager,
    private router: Router,
    private folders: FolderJsonService
  ) {
    this.mobileService.screenSize = {
      height: window.innerHeight,
      width: window.innerWidth
    };
    this.eventManager.addGlobalEventListener('window', 'resize', ($event) => {
      const height = $event.currentTarget.innerHeight;
      const width = $event.currentTarget.innerWidth;
      this.mobileService.screenSize = { height, width };
      if (this.sidenav.opened) {
        if (width > 600) {
          this.sidenav.close();
        }
      }
    });
  }
  navigate_to($event) {
    this.router.navigate([$event]);
  }
}
