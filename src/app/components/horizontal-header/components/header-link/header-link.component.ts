import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-header-link',
  templateUrl: './header-link.component.html',
  styleUrls: ['./header-link.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderLinkComponent implements OnInit {
  @Input() text: string;
  @Input() linksEnum: any;
  @Input() link: any;
  @Output() linkClick: EventEmitter<string> = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

  clicked(){
    this.linkClick.emit(this.link);
  }

}
