import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MatTabGroup } from '@angular/material/tabs';
import { HeaderLink } from './models/horizontal-header.models';
import { MobileService } from '../../services/mobile/mobile.service';
import { tap, map } from 'rxjs/operators';
import { MatSidenav } from '@angular/material/sidenav';
import { links, linkEnums } from '../../app.component';

@Component({
  selector: 'app-horizontal-header',
  templateUrl: './horizontal-header.component.html',
  styleUrls: ['./horizontal-header.component.scss']
})
export class HorizontalHeaderComponent implements OnInit {
  @Input() sidenav: MatSidenav;
  public meow = 'MEOWWWWWWWWWWWWWW';
  public links = links.filter((link) => link.value !== linkEnums.HOME);
  public mobileSize = null;
  public screenSize$ = this.mobileService.screenSize$;
  constructor(private router: Router, private mobileService: MobileService) { }

  ngOnInit() {
    // console.log('mobileService', this);
  }

  public navigate_to(whereTo: any) {
    this.router.navigate([whereTo]);
  }
}
