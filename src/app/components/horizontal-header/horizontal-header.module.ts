import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HorizontalHeaderComponent } from './horizontal-header.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HeaderLinkComponent } from './components/header-link/header-link.component';
import { MatTabsModule } from '@angular/material/tabs';
import {MatSidenavModule} from '@angular/material/sidenav';


const components = [
  HorizontalHeaderComponent,
  HeaderLinkComponent
]
@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    FlexLayoutModule,
    MatTabsModule,
    MatSidenavModule
  ],
  exports: [...components]
})
export class HorizontalHeaderModule { }
