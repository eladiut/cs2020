import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FolderModule } from '../folder/folder.module';
import { FoldersComponent } from './folders.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';


const components = [FoldersComponent]
@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    FolderModule,
    MatExpansionModule,
    MatButtonModule,
    FlexLayoutModule
  ],
  exports: [...components]
})
export class FoldersModule { }
