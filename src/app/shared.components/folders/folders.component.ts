import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { FolderJsonService } from '../../services/folder-json.service';

enum Translations {
  'CYBER' = 'סייבר',
  'COURSE_CHOICES' = 'קורסי תשתית',
  'OPTIONAL_COURSES' = 'קורסי בחירה',
  'YEAR_A' = 'שנה א\'',
  'YEAR_B' = 'שנה ב\'',
  'YEAR_C' = 'שנה ג\'',
  'SEMESTER_A' = 'סמסטר א\'',
  'SEMESTER_B' = 'סמסטר ב\''
}
@Component({
  selector: 'app-folders',
  templateUrl: './folders.component.html',
  styleUrls: ['./folders.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FoldersComponent implements OnInit {
  @Input() foldersJson: any;
  public translations = Translations;
  constructor() { }

  ngOnInit() {}

}
