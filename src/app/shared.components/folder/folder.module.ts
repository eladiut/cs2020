import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { FolderComponent } from './folder.component';
import { FlexLayoutModule } from '@angular/flex-layout';


const compoents = [FolderComponent];
@NgModule({
  declarations: [...compoents],
  imports: [
    CommonModule,
    MatButtonModule,
    MatExpansionModule,
    FlexLayoutModule
  ],
  exports: [...compoents]
})
export class FolderModule { }
