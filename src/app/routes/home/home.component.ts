import { Component, OnInit } from '@angular/core';
import { homeWelcomeText } from './home.text';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public welcomeText = homeWelcomeText;
  constructor() { }

  ngOnInit() {
  }

}
