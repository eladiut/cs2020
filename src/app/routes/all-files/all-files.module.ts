import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllFilesRoutingModule } from './all-files-routing.module';
import { AllFilesComponent } from './all-files.component';
import { FoldersModule } from '../../shared.components/folders/folders.module';


@NgModule({
  declarations: [AllFilesComponent],
  imports: [
    CommonModule,
    AllFilesRoutingModule,
    FoldersModule
  ]
})
export class AllFilesModule { }
