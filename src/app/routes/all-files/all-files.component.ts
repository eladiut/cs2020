import { Component, OnInit } from '@angular/core';
import { FolderJsonService } from '../../services/folder-json.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-all-files',
  templateUrl: './all-files.component.html',
  styleUrls: ['./all-files.component.scss']
})
export class AllFilesComponent implements OnInit {
  public allFolders = this.folderService.getFolders();
  constructor(private folderService: FolderJsonService) { }

  ngOnInit() {
  }

}
