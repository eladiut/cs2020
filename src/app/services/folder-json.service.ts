import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, pluck } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FolderJsonService {

  constructor(private http: HttpClient) {}

  public getFolders() {
    return this.http.get(`./assets/data.json?${new Date().toString()}`).pipe(pluck('FOLDERS'));
  }
}
