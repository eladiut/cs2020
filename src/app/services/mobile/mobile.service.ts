import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
export interface ScreenSize {
  width: number;
  height: number;
}
@Injectable({
  providedIn: 'root'
})
export class MobileService {
  _screenSize$: BehaviorSubject<ScreenSize> = new BehaviorSubject<ScreenSize>({ height: 0, width: 0 });
  constructor() { }
  public set screenSize(size) {
    this._screenSize$.next(size);
  }
  public get screenSize() {
    return this._screenSize$.getValue();
  }
  public get screenSize$() {
    return this._screenSize$.asObservable();
  }
}
